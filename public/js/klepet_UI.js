/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


//Ustvarimo novo funkcijo za dodajanje slik
function dodajSlike(vhodnoBesedilo) {
  //Spremenljivka, ki bo sluzila za zamenjavo vhodnega besedila
  var zacasno = vhodnoBesedilo;
  //Če se besedilo začne z http/s in konča ...gif, jpg ali png
  if(vhodnoBesedilo.search(("http://" || "https://") && (".jpg" || ".png" || ".gif"))) {
    //vhodno besedilo (ki je link-http/s )se zamenja z sliko  <br>=break ($1 je podobno kot pointer-pointer na source slike)
    zacasno = vhodnoBesedilo.replace(/((http|https):\/\/.*(\.jpg|\.png|\.gif))/, 
              vhodnoBesedilo + '<br><img style=\"width:200px; margin-left:20px;\" src=\"$1\" \/>');
  }
  return zacasno;
}
//Shranjevanje nadimka
function dodajNadimek(sporocilo) {
  var besede = sporocilo.split(" ");
  besede.shift();
  var besedilo = besede.join(' ');
  var parametri = besedilo.split('\"');
  //Kreiranje tabele
  var vzdevek = parametri[1];
  //Ukaz:/preimenuj "vzdevek" "nadimek"
  var nadimek = parametri[3];
  if (vzdevek && nadimek) {
    //Če vzdevek ni enak trenutnemu vzdevku
    if(vzdevek == trenutniVzdevek) {
      $('#sporocila').append(divElementHtmlTekst('Sebi ni mogoče dati nadimka'));     
    } //Če je
    else {
      nadimki[vzdevek] = nadimek;
      $('#sporocila').append(divElementHtmlTekst('Nadimek narejen'));
    }
  }
  else {
    $('#sporocila').append(divElementHtmlTekst('NAPAKA'));
  }
  $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = sporocilo.indexOf('<img') > -1;
  var jeKrc = sporocilo.indexOf("&#9756;") > -1;
  if (jeSmesko || jeKrc) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } 
  else if (jeSlika) {
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  
  //**************************************************************
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSlike(sporocilo);
  var sistemskoSporocilo;
  
  if(sporocilo.toLowerCase().indexOf("/preimenuj")==0) {
    dodajNadimek(sporocilo);
  }
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var nadimki = {};

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    //*****************************************
    var vzdevek = sporocilo.vzdevek;
    //Shrani sporocilo besedila 
    var besedilo = sporocilo.besedilo;
    //Preveri, če ta uporabnik ima nadimek
    if(vzdevek && nadimki[vzdevek]) {
      //Namesto vzdevka napiše nadimek, vzdevek je v oklepaju
      besedilo = besedilo.replace(vzdevek, nadimki[vzdevek] + " (" + vzdevek + ")");
    }
    //Mora biti za if-om, ker se besedilo lahko spremeni
    var novElement = divElementEnostavniTekst(besedilo);
    //Če uporabnik zamenja vzdevek
    if(sporocilo.preimenovanje) {
      var vzdevekStari = sporocilo.vzdevekStari;
      var vzdevekNovi = sporocilo.vzdevekNovi;
      nadimki[vzdevekNovi] = nadimki[vzdevekStari];
      delete nadimki[vzdevekStari];
    }
    $('#sporocila').append(novElement);
  });

  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
        var vzdevek = uporabniki[i];
        var besedilo = vzdevek;
        if(nadimki[vzdevek]) {
          besedilo = nadimki[vzdevek] + " (" + vzdevek + ")";
        }
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }

    // Krcanje
    $('#seznam-uporabnikov div').click(function() {
      klepetApp.procesirajUkaz('/zasebno ' + '"' + $(this).text()+ '"'+'"&#9756;"');
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

